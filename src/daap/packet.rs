use byteorder::{ByteOrder, NetworkEndian};

/// Four char code
pub struct FourCC {
    value: [u8; 4],
}

impl From<&[u8]> for FourCC {
    fn from(value: &[u8]) -> FourCC {
        FourCC {
            value: value.try_into().expect("less than 4 bytes"),
        }
    }
}

impl PartialEq<[u8; 4]> for FourCC {
    fn eq(&self, rhs: &[u8; 4]) -> bool {
        self.value == *rhs
    }
}

impl std::fmt::Debug for FourCC {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "'")?;
        for c in self.value {
            if c < 32 || c > 127 {
                write!(f, "\\{:x}", c)?;
            } else {
                write!(f, "{}", c as char)?;
            }
        }
        write!(f, "'")
    }
}

/// The DAAP Packet
#[derive(Debug)]
pub struct Packet {
    pub tag: FourCC,
    pub size: u32,
    data: Vec<u8>,
}

impl TryFrom<&[u8]> for Packet {
    // XXX fix this to be an error code.
    type Error = &'static str;

    fn try_from(v: &[u8]) -> Result<Packet, Self::Error> {
        if v.len() < 8 {
            return Err("Not enough data");
        }
        let tag = FourCC::from(&v[..4]);
        // It is known that sometime the size announced
        // bigger than the data received. Correct it
        let size = std::cmp::min(NetworkEndian::read_u32(&v[4..8]), v.len() as u32);
        Ok(Packet {
            tag,
            size,
            data: Vec::from(&v[8..8 + (size as usize)]),
        })
    }
}

impl Packet {
    // Return all the packet in the data.
    pub fn packets(&self) -> Vec<Packet> {
        let mut packets = vec![];
        let mut index = 0;

        while let Ok(packet) = Packet::try_from(&self.data[index..]) {
            index += packet.size as usize + 8;
            packets.push(packet);
        }
        packets
    }
}

#[cfg(test)]
mod test {
    use super::Packet;

    #[test]
    fn test_packet() {
        const PACKET_DATA: [u8; 12] = [109, 115, 116, 116, 0, 0, 0, 4, 0, 0, 0, 200];

        let packet = Packet::try_from(PACKET_DATA.as_ref());
        assert!(packet.is_ok(), "Error loading packet");
        let packet = packet.unwrap();
        assert_eq!(packet.tag, *b"mstt");
        assert_eq!(packet.size, 4);
        assert_eq!(packet.data.len(), 4);

        let packet = Packet::try_from(b"abcd".as_ref());
        assert!(packet.is_err(), "Should have been an error");
    }
}
