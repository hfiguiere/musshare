//! MusShare provide implementation for Music sharing protocols.
//!
//! It currently implements clients for DAAP (Digital Audio
//! Access Protocol) <https://en.wikipedia.org/wiki/Digital_Audio_Access_Protocol>
pub mod browser;
pub mod daap;
