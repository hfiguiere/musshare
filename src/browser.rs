//! Browser for services on the network.

pub struct Browser {}

impl Browser {
    /// Browse the available DAAP services on the network
    ///
    pub async fn browse_daap() -> Result<Vec<async_zeroconf::Service>, async_zeroconf::ZeroconfError>
    {
        let mut browser = async_zeroconf::ServiceBrowserBuilder::new("_daap._tcp");
        let mut services = browser
            .timeout(tokio::time::Duration::from_secs(2))
            .browse()?;

        let mut browse_results = vec![];
        while let Some(Ok(v)) = services.recv().await {
            let _ = async_zeroconf::ServiceResolver::r(&v).await.and_then(|r| {
                browse_results.push(r);
                Ok(())
            });
        }

        Ok(browse_results)
    }
}
