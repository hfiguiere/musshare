//! Implement DAAP
use std::str::FromStr;

use hyper::client::ResponseFuture;
use hyper::{Body, Client as HttpClient, Response, Uri};

mod packet;

const DAAP_PORT: u16 = 3689;
const DMAP_MIME: &str = "application/x-dmap-tagged";

#[derive(Debug)]
pub enum Error {
    Zeroconf(async_zeroconf::ZeroconfError),
    Hyper(hyper::Error),
}

impl From<hyper::Error> for Error {
    fn from(e: hyper::Error) -> Self {
        Error::Hyper(e)
    }
}

impl From<async_zeroconf::ZeroconfError> for Error {
    fn from(e: async_zeroconf::ZeroconfError) -> Self {
        Error::Zeroconf(e)
    }
}

pub type Result<T> = std::result::Result<T, Error>;

pub struct Client {
    uri: Uri,
    client: HttpClient<hyper::client::HttpConnector, hyper::Body>,
}

impl Client {
    /// Create a new DAAP client connecting to `fqdn`
    /// An optional `port` can be specified, otherwise `DAAP_PORT`
    /// is used.
    pub fn new(fqdn: &str, port: Option<u16>) -> Client {
        let uri = Uri::builder()
            .scheme("http")
            .authority(format!("{}:{}", fqdn, port.unwrap_or(DAAP_PORT)))
            .path_and_query("/")
            .build()
            .unwrap();
        Client {
            uri,
            client: HttpClient::builder().build_http(),
        }
    }

    /// Query a path from the DAAP client
    pub async fn get_path(&self, path_and_query: &str) -> ResponseFuture {
        use http::uri::{Parts, PathAndQuery};

        let mut parts = Parts::from(self.uri.clone());
        parts.path_and_query = PathAndQuery::from_str(path_and_query).ok();
        let uri = Uri::from_parts(parts).unwrap();
        self.client.get(uri)
    }

    pub fn check_mime_type(resp: &Response<Body>) -> bool {
        resp.headers()
            .get("Content-Type")
            .and_then(|value| value.to_str().ok())
            .map(|s| s == DMAP_MIME)
            .unwrap_or(false)
    }

    pub async fn decode_response(&self, res: Response<Body>) -> Result<packet::Packet> {
        let bytes = hyper::body::to_bytes(res.into_body()).await?;
        // XXX fix this
        let packet = packet::Packet::try_from(bytes.as_ref()).expect("packet parsing error");
        Ok(packet)
    }
}
