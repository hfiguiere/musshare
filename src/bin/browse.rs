use hyper::http::StatusCode;

use musshare::browser;
use musshare::daap;

#[tokio::main]
async fn main() -> Result<(), daap::Error> {
    if let Ok(s) = browser::Browser::browse_daap().await {
        for service in s {
            let host = service.host().as_deref().unwrap_or("*");
            let domain = service.domain().as_deref().unwrap_or("local.");
            println!("{}.{}:{} = {}", host, domain, service.port(), service.txt());

            let client = daap::Client::new(&format!("{}.{}", host, domain), Some(service.port()));
            let res = client.get_path("/content-codes").await;
            let res = res.await?;
            if res.status() == StatusCode::OK {
                if daap::Client::check_mime_type(&res) {
                    let packet = client.decode_response(res).await?;
                    if packet.tag == *b"mccr" {
                        println!("correct type");
                    }
                    println!("Result {:?}", packet);
                } else {
                    println!("Wrong type");
                }
            }
            break;
        }
    }

    Ok(())
}
