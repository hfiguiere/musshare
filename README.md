musshare
========

musshare stands for Music Share. This crate is meant to provide a Rust
implementation of Music sharing protocols.

It currently implement the following clients:

- DAAP

Maintainer
----------

Hubert Figuiere <hub@figuiere.net>